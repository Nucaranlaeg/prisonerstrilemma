class Grudger:
    def __init__(self):
        self.history = []
    def round(self, last):
        if(last):
            self.history.append(last)
            if(self.history.count("D") > 0):
                return "D"
        return "C"

class FastGrudger:
	def __init__(self):
		self.grudge = False
	def round(self, last):
		if(last == "D"):
			self.grudge = True
		if(self.grudge):
			return "D"
		else:
			return "C"
